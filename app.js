/*   
**  Module dependencies
** Everlive and Express
**
​*/
var Everlive = require('everlive-sdk');
var http = require('http');
var request = require('request'); // npm install request
var fs = require('fs'); // npm install fs
var buffer =  require('buffer'); //npm install buffer

//APP_ID for the old and new database from telerik backend Service Platform
var APP_ID_OLD = 'cl8mwlp8x7hrotvp';
var APP_ID_NEW = 'lacs5sag3ylbwzm7';
var SCHEME = 'https';


var _el = new Everlive({
      appId: APP_ID_OLD,
      scheme: SCHEME
});

var MIMES = {
	"image/png": 'png',
	"image/jpeg": 'jpg'
}


// Created a folder in local to download all the files using CreateWriteStream
var getImages = function() {
    
        console.log('Starting Function : getImages');
      
        if(!fs.existsSync("EverliveDownloads")) {
            console.log(' \'EverliveDownloads\' Folder does not exist, Creating it now! ');
            fs.mkdirSync("EverliveDownloads", 0766, function(err) {
                if(err) {
                    console.log('Error while creating Folder' , err);
                    throw err;
                }
                console.log(' \'EverliveDownloads\' Created Successfully ');
            });
        }
        
	var ImageQuery = new Everlive.Query();
	ImageQuery.where().ne('image','').done().select('image');
	return _el.data('Deals').get(ImageQuery).then(function(data) {
            console.log( 'Data Received : ', data.count + ' Items');
            return data.result;
	}).then(function(Result) {
            console.log('Starting ForEach');
		Result.forEach(function(Item,Key) {
			_el.files.getDownloadUrlById(Item.image).then(function(ImageUrl) {
                       //     console.log('Image Url Received For Image ID : ', Item.image );
                          var ImageRequest = request(ImageUrl);
                           
		ImageRequest.on('response', function(response) {
           var FileName = Item.image + '.' + MIMES[response.headers['content-type']];
           /****Use WriteStream to get the files and then read each file */
          //  var File = fs.createWriteStream("EverliveDownloads/" +FileName);
            // console.log( FileName + ' Downloaded Successfully' );
            // console.log('Before Base64');
            // response.pipe(File);
       
              //Read the files from the folder synchronously and convert to base64
             var readFile =  fs.readFileSync('EverliveDownloads/'+FileName);
             var base64String = readFile.toString('base64');
              console.log('************' + Key + ' ---- ' + FileName + ' Converted to base64 *************');
              console.log('\n'+ FileName + ' contents are: ' + base64String);
              /**********************Started Uploading**************** */
              
              
              var _el = new Everlive({
             appId: APP_ID_NEW,
             scheme: SCHEME
             });
              console.log('Uploading Started for file : ' +FileName);
     
     //Upload the files one by one using base64 format iterating through the files          
     var file = {
    "Filename": FileName,
    "ContentType": response.headers['content-type'],
     "base64": base64String
};
        _el.Files.create(file,function(data){
            console.log('The file name is : ' + FileName);
            console.log('Uploaded Successfully!!!' + FileName);
        },function(err){
            console.log('AWWWWW',err);
            throw err;
        });
              
            });
            
           //});
              
         
             //});
   
        ImageRequest.on('error', function(err) {
                                    console.log('Oh, Bother!', err);
                                    throw err;
                  });
			}, function(err) {
                            console.log('Aw crap!', err);
                            throw err;
             });
		});
	}, function(err) {
            console.log('Oh no!', err);
            throw err;
        });
}


/**************Getting Logo Images from NearMe Table***************** */

var getLogo = function() {
    
        console.log('Starting Function : getLogo');
        
        if(!fs.existsSync("EverliveDownloadsLogo")) {
            console.log(' \'EverliveDownloadsLogo\' Folder does not exist, Creating it now! ');
            fs.mkdirSync("EverliveDownloadsLogo", 0766, function(err) {
                if(err) {
                    console.log('Error while creating Folder' , err);
                    throw err;
                }
                console.log(' \'EverliveDownloadsLogo\' Created Successfully ');
            });
        }
        
	var LogoQuery = new Everlive.Query();
	LogoQuery.where().ne('Logo','').done().select('Logo');
	return _el.data('NearMe').get(LogoQuery).then(function(data) {
            console.log( 'Data Received : ', data.count + ' Items');
            return data.result;
	}).then(function(Result) {
            console.log('Starting ForEach');
		Result.forEach(function(Item,Key) {
			_el.files.getDownloadUrlById(Item.Logo).then(function(ImageUrl) {
                       //     console.log('Image Url Received For Image ID : ', Item.image );
                          var ImageRequest = request(ImageUrl);
                            
		ImageRequest.on('response', function(response) {
           var FileName = Item.Logo + '.' + MIMES[response.headers['content-type']];
         //  var File = fs.createWriteStream("EverliveDownloadsLogo/" +FileName);
            // console.log( FileName + ' Downloaded Successfully' );
           //  console.log('Before Base64');
            // response.pipe(File);
             
           
             var readFile =  fs.readFileSync('EverliveDownloadsLogo/'+FileName);
             var base64String = readFile.toString('base64');
              console.log('************' + Key + ' ---- ' + FileName + ' Converted to base64 *************');
              console.log('\n'+ FileName + ' contents are: ' + base64String);
              /**********************Started Uploading**************** */
              
              var _el = new Everlive({
             appId: APP_ID_NEW,
             scheme: SCHEME
             });
              console.log('Uploading Started for file : ' +FileName);
               
     var file = {
    "Filename": FileName,
    "ContentType": response.headers['content-type'] ,
     "base64": base64String
};
        _el.Files.create(file,function(data){
            console.log('The file name is : ' + FileName);
            console.log('Uploaded Successfully!!!' + FileName);
        },function(err){
            console.log('AWWWWW',err);
            throw err;
        });
              
            });
            
      //  });
        ImageRequest.on('error', function(err) {
                                    console.log('Oh, Bother!', err);
                                    throw err;
                  });
			}, function(err) {
                            console.log('Aw crap!', err);
                            throw err;
             });
		});
	}, function(err) {
            console.log('Oh no!', err);
            throw err;
        });
}




/** Will Download all the Files it gets and Store them as their ID **/
getImages()
getLogo()


